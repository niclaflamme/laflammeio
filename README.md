# React-Redux Boilerplate
This is a react-redux boilerplate.
GIANT WIP.


## State

### Definitions
- `action` - an object with a `type` property and an optionnal `payload` property that is passed to the reducer.
- `operation` - an operation can be viewed as a complex action. For example, it can be used for async stuff and will itself dispatch one or multiple actions.
- `reducer` - A pure function that takes the current state and an action and returns the new application's state.
- `selector` - A function that takes the applications state an returns a value from it. You may perform calculations on the state.
- `types` - a collecion of variables of type string to prevent typos.
- `Record` - This is an immutable object with fixed properties and default values. From Facebook's `immutable` library.

### File structure
The application state is organized by domain rather than being flat at the application root folder.
As the application scales it will maked finding parts of the state more straightfowards.
Tests should also be located inside their respective domain folders.

```yml
// GOOD
src
  components
  containers
  hocs
  state
    authentication
      actions
      operations
      reducers
      types
      selectors
    articles
      actions
      operations
      reducers
      types
      selectors
    users
      actions
      operations
      reducers
      types
      selectors

// BAD
src
  actions
    authentication
    articles
    users
  types
    authentication
    articles
    users
  components
  containers
  hocs
  operations
    authentication
    articles
    users
  reducers
    authentication
    articles
    users
  selectors
    authentication
    articles
    users
```

### Flow
Flow should be enabled throughout the state folder to catch errors


## Components

### Definitions
- `component` - A styled rendering of props. A component is not connected to the Redux store and has no children connected to the store either. It can either be created from the react library or from styled components.
- `containers` - A component connected to the Redux store or a component with children that are containers or hocs.
- `hocs` - Higher Order Component. A function that takes a component as an argument and returns another decorated component.

### Convenience scripts (WIP)
There are two convenience script to create the basic file structure for a new component or container.

### Props
Flow is not enabled inside of the components folders. This is because it does not currently play well with the Immutable data structure used for properties. PropTypes, AirBnb-PropTypes and AirBnb's Eslint do a great job as it is.

Containers and HoC should not contain any objects or arrays as props. Favor `Records`, `Maps` and `Lists`.

Components should only contain native Javascript primitives props.


## Styling
### Styled-Components
There is no css file in the application rather the components and containers are styled via the `styled-components` library in Javascript with the style living in the JSX file.

### Theme
As a replacement for SCSS or Sass variables, styled that are ment to be duplicated should be saved inside the `theme` folder and imported. Thus allowing a single to change to propagate inside the entire application.

### JSX folder styled guide
- Import library variables first
- Skip a line
- Import local variables
- Skip two lines
- Create Styled-Components variables
- Skip one line in between of every Styled-Component
- Skip two lines
- Build Component

### Nice to have
Local variables can be imported via an alias that is resolved by webpack
`import { Title } from 'components';`
