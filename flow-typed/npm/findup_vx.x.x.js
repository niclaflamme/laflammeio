// flow-typed signature: 9aa24d5526c66084e01d8c705ae45782
// flow-typed version: <<STUB>>/findup_v0/flow_v0.64.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'findup'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'findup' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'findup/bin/findup' {
  declare module.exports: any;
}

declare module 'findup/test/findup-test' {
  declare module.exports: any;
}

// Filename aliases
declare module 'findup/bin/findup.js' {
  declare module.exports: $Exports<'findup/bin/findup'>;
}
declare module 'findup/index' {
  declare module.exports: $Exports<'findup'>;
}
declare module 'findup/index.js' {
  declare module.exports: $Exports<'findup'>;
}
declare module 'findup/test/findup-test.js' {
  declare module.exports: $Exports<'findup/test/findup-test'>;
}
