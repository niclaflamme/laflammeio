#!/bin/bash

usage() {
  echo
  echo "Generates the files and folder structure for a new Container"
  echo
  echo "  ./container.sh NewContainer"
  echo
  echo
  echo "  Arguments:"
  echo "      ContainerName           The name of your container in PascalCase"
  echo
  echo
}

CONTAINER_NAME=$1

CONTAINERS_BASE_FOLDER="src/containers"
CONTAINER_FOLDER="${CONTAINERS_BASE_FOLDER}/${CONTAINER_NAME}"
CONTAINER_JSX_FILE_PATH="${CONTAINER_FOLDER}/index.jsx"
CONTAINER_README_FILE_PATH="${CONTAINER_FOLDER}/Readme.md"
CONTAINER_TEST_FOLDER="${CONTAINER_FOLDER}/__tests__"
CONTAINER_TEST_FILE_PATH="${CONTAINER_TEST_FOLDER}/index.jsx"

if [[ -z "$CONTAINER_NAME" ]];
then
  usage
  echo
  echo "ERROR - Missing container name."
  exit 1
fi

if [[ ${CONTAINER_NAME:0:1} =~ [a-z] ]];
then
  usage
  echo
  echo "ERROR - Expected capitalized container name."
  exit 1
fi

create_container_directories() {
    mkdir -p ${CONTAINER_TEST_FOLDER}
    echo "Created directories."
}

create_container_files() {
cat << EOF > ${CONTAINER_JSX_FILE_PATH}
import React, { Component } from 'react';
import { connect } from 'react-redux';


class ${CONTAINER_NAME} extends Component {
  static defaultProps = {};

  render() {
    return <div />;
  }
}

export default connect()(${CONTAINER_NAME});
EOF

    echo "Created container ${CONTAINER_JSX_FILE_PATH}."
}

add_container_to_index() {
cat << EOF >> ${CONTAINERS_BASE_FOLDER}/index.js
export { default as ${CONTAINER_NAME} } from './${CONTAINER_NAME}';
EOF

    echo "Added to containers/index.js"
}

add_jest_boilerplate() {
cat << EOF > ${CONTAINER_TEST_FILE_PATH}
import React from 'react';
import { Provider } from 'react-redux';
import { shallow } from 'enzyme';

import { ${CONTAINER_NAME} } from 'containers';
import store from 'state';


test('renders the container', () => {
  const wrapper = shallow(
    <Provider store={store}>
      <${CONTAINER_NAME} />
    </Provider>,
  );
  expect(wrapper.length).toEqual(1);
});
EOF

    echo "Created Jest boilerplate for ${CONTAINER_TEST_FILE_PATH}."
}

add_readme() {
    touch $CONTAINER_README_FILE_PATH
    echo "Created container Readme"
}


# MAIN
create_container_directories
create_container_files
add_container_to_index
add_jest_boilerplate
add_readme

echo "Completed."
exit 0
