#!/bin/bash

usage() {
  echo
  echo "Generates the files and folder structure for a new Component"
  echo
  echo "  ./component.sh NewComponent --statefull"
  echo
  echo
  echo "  Arguments:"
  echo "      ComponentName           The name of your component in PascalCase"
  echo
  echo
}

COMPONENT_NAME=$1

COMPONENTS_BASE_FOLDER="src/components"
COMPONENT_FOLDER="${COMPONENTS_BASE_FOLDER}/${COMPONENT_NAME}"
COMPONENT_JSX_FILE_PATH="${COMPONENT_FOLDER}/index.jsx"
COMPONENT_README_FILE_PATH="${COMPONENT_FOLDER}/Readme.md"
COMPONENT_TEST_FOLDER="${COMPONENT_FOLDER}/__tests__"
COMPONENT_TEST_FILE_PATH="${COMPONENT_TEST_FOLDER}/index.jsx"

if [[ -z "$COMPONENT_NAME" ]];
then
  usage
  echo
  echo "ERROR - Missing component name."
  exit 1
fi

if [[ ${COMPONENT_NAME:0:1} =~ [a-z] ]];
then
  usage
  echo
  echo "ERROR - Expected capitalized component name."
  exit 1
fi

create_component_directories() {
    mkdir -p ${COMPONENT_TEST_FOLDER}
    echo "Created directories."
}

add_component_to_index() {
cat << EOF >> ${COMPONENTS_BASE_FOLDER}/index.js
export { default as ${COMPONENT_NAME} } from './${COMPONENT_NAME}';
EOF

    echo "Added to components/index.js"
}

create_stateless_component() {
cat << EOF >> ${COMPONENT_JSX_FILE_PATH}
import React from 'react';


const ${COMPONENT_NAME} = () => <div />;

${COMPONENT_NAME}.defaultProps = {};

export default ${COMPONENT_NAME};
EOF

    echo "Created stateless component ${COMPONENT_JSX_FILE_PATH}."
}

add_jest_boilerplate() {
cat << EOF > ${COMPONENT_TEST_FILE_PATH}
import React from 'react';
import { shallow } from 'enzyme';

import { ${COMPONENT_NAME} } from 'components';


test('renders the component', () => {
  const wrapper = shallow(<${COMPONENT_NAME} />);
  expect(wrapper.length).toEqual(1);
});
EOF

    echo "Created Jest boilerplate for ${COMPONENT_TEST_FILE_PATH}."
}

add_readme() {
    touch $COMPONENT_README_FILE_PATH
    echo "Created component Readme"
}


# MAIN
create_component_directories
create_stateless_component
add_component_to_index
add_jest_boilerplate
add_readme

echo "Completed."
exit 0
