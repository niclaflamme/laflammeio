const path = require('path');
const paths = require('../paths');

module.exports = {
  assets: path.resolve(paths.appSrc, 'assets'),
  components: path.resolve(paths.appSrc, 'components'),
  containers: path.resolve(paths.appSrc, 'containers'),
  hocs: path.resolve(paths.appSrc, 'hocs'),
  state: path.resolve(paths.appSrc, 'state'),
  theme: path.resolve(paths.appSrc, 'theme'),
};

