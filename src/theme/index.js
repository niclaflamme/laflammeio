import * as color from './color';
import * as font from './font';

export {
  color,
  font,
};
