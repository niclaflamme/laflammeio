// @flow
/* eslint-disable sort-keys */

export const family = {
  sansSerif: '\'Roboto\', sans-serif;',
  mono: '\'Roboto Mono\', monospace;',
};

export const lineHeight = {
  h1: '52px',
  h2: '48px',
  h3: '40px',
  h4: '32px',
  h5: '28px',
  h6: '22px',
  p: '22px',
};

export const size = {
  h1: '44px',
  h2: '40px',
  h3: '32px',
  h4: '24px',
  h5: '20px',
  h6: '14px',
  p: '14px',
};

export const weight = {
  light: 300,
  regular: 400,
  bold: 700,
};
