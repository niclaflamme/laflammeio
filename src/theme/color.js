// @flow
/* eslint-disable sort-keys */

export const brand = {
  primary: '#292f36',
  secondary: '#3c464f',
  accent: '#ff6b6b',
  faded: '#ccc5b9',
  light: '#ffffff',
};

export const text = {
  primary: brand.primary,
  accent: brand.accent,
  disabled: brand.faded,
  light: brand.light,
};
