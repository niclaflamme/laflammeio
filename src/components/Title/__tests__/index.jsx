// @flow
import * as React from 'react';
import { shallow } from 'enzyme';

import { Title } from 'components';


test('renders the component', () => {
  const wrapper = shallow(<Title>title</Title>);

  expect(wrapper.length).toEqual(1);
});

test('default titles are h2', () => {
  const wrapper = shallow(<Title>title</Title>);
  const header = wrapper.render()[0];

  expect(header.name).toEqual('h2');
});

test('large titles are h1', () => {
  const wrapper = shallow(<Title large>large</Title>);
  const header = wrapper.render()[0];

  expect(header.name).toEqual('h1');
});

test('small titles are h3', () => {
  const wrapper = shallow(<Title small>large</Title>);
  const header = wrapper.render()[0];

  expect(header.name).toEqual('h3');
});
