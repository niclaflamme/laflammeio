import PropTypes from 'prop-types';
import React, { Component } from 'react';
import styled from 'styled-components';

import { font } from 'theme';


const getStyledElement = nodeType => styled[nodeType]`
  margin: 0;

  font-family: ${font.family.sansSerif};
  font-size: ${font.size[nodeType]};
  line-height: ${font.lineHeight[nodeType]};

  font-weight: ${font.weight.regular};
`;


class Title extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    large: PropTypes.bool,
    small: PropTypes.bool,
  }

  static defaultProps = {
    large: false,
    small: false,
  }

  getElement = () => {
    const { large, small } = this.props;

    if (large) {
      return getStyledElement('h1');
    }

    if (small) {
      return getStyledElement('h3');
    }

    return getStyledElement('h2');
  }

  render() {
    const { children } = this.props;
    const Element = this.getElement();

    return <Element>{ children }</Element>;
  }
}

export default Title;
