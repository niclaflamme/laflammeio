// @flow
import { Record } from 'immutable';
import { combineReducers } from 'redux-immutable';

import display, { State as DisplayState } from './display';


export const State = Record({
  display: DisplayState(),
});

export default combineReducers({
  display,
});
