// @flow
import { Record } from 'immutable';
import { combineReducers } from 'redux-immutable';

import { SET_SIZE } from './types';


export const State = Record({
  height: 0,
  width: 0,
});

const height = (state = State().height, action) => {
  switch (action.type) {
    case SET_SIZE:
      return action.payload.height;
    default:
      return state;
  }
};

const width = (state = State().width, action) => {
  switch (action.type) {
    case SET_SIZE:
      return action.payload.width;
    default:
      return state;
  }
};

export default combineReducers({
  height,
  width,
});
