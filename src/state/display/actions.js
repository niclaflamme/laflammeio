// @flow
import { SET_SIZE } from './types';


// eslint-disable-next-line import/prefer-default-export
export const updateWindowSize = () => {
  const height = window.innerHeight;
  const width = window.innerWidth;

  return {
    payload: { height, width },
    type: SET_SIZE,
  };
};
