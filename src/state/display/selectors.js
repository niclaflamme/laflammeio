export const getHeight = state => state.display.height;
export const getWidth = state => state.display.width;
