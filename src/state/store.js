// @flow
import thunk from 'redux-thunk';
import { applyMiddleware, compose, createStore } from 'redux';

import { DevTools } from 'containers';
import rootReducer, { State } from './reducers';


const middleware = applyMiddleware(thunk);

const enhancers = process.env.REACT_APP_DEBUG ?
  compose(middleware, DevTools.instrument()) :
  compose(middleware);

const store = createStore(rootReducer, State(), enhancers);

export default store;
