// @flow
import { compose } from 'redux';

import DevTools from '../containers/DevTools';
import middleware from './middleware';


const enhancers = process.env.REACT_APP_DEBUG ?
  compose(middleware, DevTools.instrument()) :
  compose(middleware);

export default enhancers;
