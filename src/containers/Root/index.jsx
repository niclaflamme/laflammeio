import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';

import { App, DevTools } from 'containers';
import { actions } from 'state';


class Root extends Component<void> {
  componentDidMount() {
    this.props.updateWindowSize();
    window.addEventListener('resize', this.updateDimentions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimentions);
  }

  updateDimentions = () => {
    this.props.updateWindowSize();
  }

  render() {
    return (
      <div>
        <Router>
          <App />
        </Router>
        <DevTools />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  updateWindowSize: () => dispatch(actions.display.updateWindowSize()),
});

export default connect(null, mapDispatchToProps)(Root);
