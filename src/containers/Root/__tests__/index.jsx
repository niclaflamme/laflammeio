// @flow
import * as React from 'react';
import { shallow } from 'enzyme';
import { Provider } from 'react-redux';

import { Root } from 'containers';
import store from 'state';


test('renders the component', () => {
  const wrapper = shallow(
    <Provider store={store}>
      <Root />
    </Provider>,
  );
  expect(wrapper.length).toEqual(1);
});
