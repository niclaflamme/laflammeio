import React from 'react';
import { integer } from 'airbnb-prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { Title } from 'components';
import { selectors } from 'state';
import { color } from 'theme';


const Content = styled.div`
  padding-top: 50px;

  text-align: center;

  background-color: ${color.brand.light};
`;


const App = ({ height, width }) => (
  <Content>
    <Title small>dimentions</Title>
    <p>
      <strong>height:</strong> { ' ' }
      { height }
    </p>
    <p>
      <strong>width:</strong> { ' ' }
      { width }
    </p>
  </Content>
);

App.propTypes = {
  height: integer.isRequired,
  width: integer.isRequired,
};

const mapStateToProps = state => ({
  height: selectors.display.getHeight(state),
  width: selectors.display.getWidth(state),
});

export default connect(mapStateToProps)(App);
