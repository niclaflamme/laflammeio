import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import { Root } from 'containers';
import store from 'state';
import registerServiceWorker from './registerServiceWorker';
import './index.css';

const node = (
  <Provider store={store}>
    <Root />
  </Provider>
);

const mount = document.getElementById('root');

render(node, mount);
registerServiceWorker();
