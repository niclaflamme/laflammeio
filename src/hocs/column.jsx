import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { actions, selectors } from 'state';

const column = index => WrappedComponent => {
  class FocusableComponent extends Component {
    static propTypes = {
      focusColumn: PropTypes.func.isRequired,
    }

    render() {
      return (
        <WrappedComponent
          {...this.props}
          onClick={this.handleOnClick}
        />
      );
    }
  }

  const mapStateToProps = state => ({
    isFocused: selectors.navigation.isFocused(state, index),
  });

  const mapDispatchToProps = dispatch => ({
    focusColumn: () => dispatch(actions.navigation.focusColumn(index)),
  });

  return connect(mapStateToProps, mapDispatchToProps)(FocusableComponent);
};

export default column;
